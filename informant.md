# ADHD Informant Report Questionnaire

Inattention symptoms

In each question please indicate NEVER RARELY SOMETIMES OFTEN VERY OFTEN  THEN  If “often” or "very often” PLEASE GIVE DESCRIPTIVE EXAMPLES FROM BOTH childhood AND ADULTHOOD

1. Does he/she fail to give close attention to details or make careless mistakes in schoolwork, at work, or with other activities?
2. Do they often have trouble holding their attention on tasks?
3. Do they often find it difficult to listen when spoken to directly?
4. Do they often find it difficult to follow through on instructions and fail to finish tasks because they get side tracked or distracted?
5. Do they often have trouble organizing tasks and activities?
6. Do they often avoid, dislike, or are reluctant to do tasks that require mental effort over a long period of time (such as schoolwork or homework)?
7. Do they often lose things necessary for tasks and activities (e.g. school materials, pencils, books, tools, wallets, keys, paperwork, glasses, mobile telephones)?
8. Are they often easily distracted?
9. Are they often forgetful in daily activities?

Part 2 - Hyperactivity and Impulsivity Symptoms

1. Does he/she often fidget, tap their hands or feet, or squirm in their seat?
2. Do they often leave their seat in situations when remaining seated is expected?
3. Do you think they often feel restless if they have to remain still?
4. Is it often hard for them to take part in leisure activities quietly?
5. Do they often act as if they are always on the go?
6. Do they often talk excessively?
7. Do they often blurt out an answer before a question has been completed?
8. Do they often have trouble waiting for their turn?
9. Do they often interrupt or intrude on others (e.g., butting into conversations or games)?

Areas affected
Please indicate 'YES' to the area(s) of your life which are most affected by your symptoms:

- Education
- Employment
- Emotional Health (e.g. self-esteem)
- Family Relationships
- Finance
- Friendship
- Leisure time/ Relaxation/ Sleep
