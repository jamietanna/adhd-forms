In each question please indicate
NEVER RARELY SOMETIMES OFTEN VERY OFTEN

1. How often do you have trouble wrapping up the final details of a project, once the challenging parts have been done?
1. How often do you have difficulty getting things in order when you have to do a task that requires organisation?
1. How often do you have problems remembering appointments and obligations?
1. When you have a task that requires a lot of thought, how often do you avoid or delay getting started?
1. How often do you squirm or fidget with your hands or feet when you have to sit down for a long period of time?
1. How often do you feel overly active and compelled to do things, like you were driven by a motor
1. How often do you make careless mistakes when you have to work on a boring or difficult project?
1. How often do you have difficulty keeping your attention when you are doing boring or repetitive work?
1. How often do you have difficulty concentrating on what people say to you, even when they are speaking to you directly?
1. How often do you misplace or have difficulty finding things at home or work?
1. How often are you distracted by activity or noise around you?
1. How often do you leave your seat in meetings or other situations in which you are expected to remain seated?
1. How often do you feel restless or fidgety?
1. How often do you have difficulty unwinding and relaxing when you have time to yourself?
1. How often do you find yourself talking too much when you are in social situations?
1. When you’re in a conversation, how often do you find yourself finishing the sentences of people you are talking to, before they can finish them themselves?
1. How often do you have difficulty waiting your turn in situations where turn taking is required?
1. How often do you interrupt others when they are busy?
