# ADHD Informant Report Questionnaire

Part 1 - Inattention symptoms

In each question please indicate

NEVER RARELY SOMETIMES OFTEN VERY OFTEN

THEN

If “often” or "very often” PLEASE GIVE DESCRIPTIVE EXAMPLES FROM BOTH childhood and adulthood

1. Do you fail to give close attention to details or make careless mistakes in schoolwork, at work, or with other activities?
2. Do you have trouble holding your attention on tasks?
3. Do you find it difficult to listen when spoken to directly?
4. Do you find it difficult to follow through on instructions and fail to finish tasks because you get side-tracked or distracted?
5. Do you have trouble organizing tasks and activities?
6. Do you avoid, dislike, or are reluctant to do tasks that require mental effort over a long period of time (such as schoolwork or homework)?
7. Do you lose things necessary for tasks and activities (e.g. school materials, pencils, books, tools, wallets, keys, paperwork, glasses, mobile telephones)?lc
8. Are you easily distracted?
9. Are you forgetful in daily activities?

Part 2 - Hyperactivity and Impulsivity Symptoms

1. Do you fidget, tap your hands or feet, or squirm in your seat?
2. Do you leave your seat in situations when remaining seated is expected?
3. Do you feel restless if you have to remain still?
4. Is it hard for you to take part in leisure activities quietly?
5. Do you feel as if you are always on the go?
6. Do you talk excessively?
7. Do you blurt out an answer before a question has been completed?
8. Do you have trouble waiting for your turn?
9. Do you interrupt or intrude on others (e.g., butting into conversations or games)?

Areas affected (yes/no)

- Education
- Employment
- Emotional Health (e.g. self-esteem)
- Family Relationships
- Finances
- Friendships
- Leisure time/ relaxation/ sleep
